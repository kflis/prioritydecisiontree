/*
	Copyright Kevin Storchan-Flis
	11/12/2013
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Used by {@link PriorityDecisionTree} to parse text files into {@link Evidence}
 */
public class TextToEvidence {

    /**
     * Stores the final resulting {@link Evidence} lists after reading text file(s)
     */
    public static class ResultsContainer {
        public final List<Evidence> startingEvidence;
        public final List<Evidence> goalEvidence;
        public ResultsContainer(List<Evidence> startingEvidence, List<Evidence> goalEvidence) {
            this.startingEvidence = startingEvidence;
            this.goalEvidence = goalEvidence;
        }
    }

    /**
     * Helper class to store {@link Evidence} along with its index position its Evidence list.
     * Once all Evidences are processed after reading a text file, the dependencies of each evidence can then be
     * mapped using array indices rather string look ups.
     */
    private static class EvidenceAndDependencies implements Comparable<EvidenceAndDependencies> {
        public final Evidence evidence;
        public final List<DependencyAttributes> dependencies;
        public int index = -1;
        public EvidenceAndDependencies(Evidence evidence, List<DependencyAttributes> dependencies) {
            this.evidence = evidence;
            this.dependencies = dependencies;
        }

        /**
         * Set the dependencies for each {@link Evidence} according to what was read in a text file. Using data from
         * {@link EvidenceAndDependencies}, the array indices of all of the dependencies for each evidence are stored
         * for fast look up.
         *
         * @param evidenceMap maps evidence names to a container where its indexed array location is
         */
        public void setDependencies(HashMap<String, EvidenceAndDependencies> evidenceMap) {
            if (dependencies != null) {
                ArrayList<Evidence.Dependency> dependencyArray = new ArrayList<Evidence.Dependency>();
                for (DependencyAttributes pair : dependencies) {
                    EvidenceAndDependencies ead = evidenceMap.get(pair.dependency);
                    if (ead != null) {
                        int index = ead.index;
                        if (pair.type.equals("requirement"))
                            dependencyArray.add(new Evidence.Dependency(index, pair.value, true, false));
                        else
                            if (pair.type.equals("corequirement"))
                                dependencyArray.add(new Evidence.Dependency(index, pair.value, false, true));
                        else
                                if (pair.type.equals("co/requirement")) //requirement or corequirement
                                    dependencyArray.add(new Evidence.Dependency(index, pair.value, true, true));
                    }
                }
                evidence.setDependencies(dependencyArray);
            }
        }

        @Override
        public int hashCode() {
            return evidence.getName().hashCode();
        }

        @Override
        public int compareTo(EvidenceAndDependencies o) {
            return evidence.getName().compareTo(o.evidence.getName());
        }
    }

    /**
     * Container to store necessary dependency attributes for {@link Evidence} while reading a text file
     * for evidence. After the evidences are read, these containers can be used to then set up the dependencies
     * for each evidence.
     */
    public static class DependencyAttributes {
        public final String dependency;
        public final boolean value;
        public final String type;
        public DependencyAttributes(String dependency, String type, boolean value) {
            this.dependency = dependency;
            this.value = value;
            this.type = type;
        }
    }

    /**
     * Create starting and goal evidence lists from a text file
     * @param goalPath the location of a goal evidence file
     * @return a container containing starting evidence and goal evidence
     */
    public static ResultsContainer getEvidenceFromFile(String goalPath) {
        return getEvidenceFromFile(null, goalPath);
    }

    /**
     * Create starting and goal evidence lists from a text file with starting evidence existence specified
     * @param startPath the location of a starting evidence file
     * @param goalPath the location of a goal evidence file
     * @return  a container containing starting evidence and goal evidence
     */
    public static ResultsContainer getEvidenceFromFile(String startPath, String goalPath) {
        List<Evidence> goalEvidence = new ArrayList<Evidence>();
        HashMap<String, EvidenceAndDependencies> evidenceMap = new HashMap<String, EvidenceAndDependencies>();
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(goalPath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        String line = null;
        try {
            line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        while (line != null) {
            if (line.trim().equals("#EVIDENCE#"))
                try {
                    EvidenceAndDependencies ead = buildEvidence(br);
                    if (ead != null && evidenceMap.get(ead.evidence.getName()) == null) {
                        ead.index = goalEvidence.size();
                        goalEvidence.add(ead.evidence);
                        evidenceMap.put(ead.evidence.getName(), ead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        for (Evidence evidence : goalEvidence) {
            EvidenceAndDependencies ead = evidenceMap.get(evidence.getName());
            ead.setDependencies(evidenceMap);
        }

        List<Evidence> startingEvidence = new ArrayList<Evidence>();
        for (Evidence e : goalEvidence)
            startingEvidence.add(e.setExistence(false));

        if (startPath != null) {
            try {
                br = new BufferedReader(new FileReader(startPath));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }

            try {
                line = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }

            while (line != null) {
                String[] tokens = line.trim().split("=");
                if (tokens.length == 2) {
                    EvidenceAndDependencies ead = evidenceMap.get(tokens[0]);
                    try {
                        if (ead != null) {
                            Boolean value = Boolean.valueOf(tokens[1]);
                            startingEvidence.set(ead.index, ead.evidence.setExistence(value));
                        }
                    } catch (NumberFormatException e) {
                       e.printStackTrace();
                       return null;
                    }
                }
                try {
                    line = br.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

        return new ResultsContainer(startingEvidence, goalEvidence);
    }

    /**
     * Builds a {@link EvidenceAndDependencies} by parsing text
     * @param br the buffered reader we're using to read
     * @return our constructed Evidence and its unimplemented dependencies
     * @throws IOException
     */
    private static EvidenceAndDependencies buildEvidence(BufferedReader br) throws IOException {
        String name = null;
        double weight = 1;
        double bias = 1;
        List<Evidence.Interval> intervals = null;
        boolean necessary = false;
        boolean exists = true;
        List<DependencyAttributes> dependencies = null;


        String nameIdentifier = "name";
        String weightIdentifier = "weight";
        String biasIdentifier = "bias";
        String existsIdentifier = "exists";
        String necessaryIdentifier = "necessary";
        String intervalsIdentifier = "intervals";
        String dependenciesIdentifier = "dependencies";

        String line = br.readLine();
        while (line != null && !line.trim().equals("#END#")) {
            line = line.trim();
            String [] tokens = line.split("=");
            if (tokens.length == 2) {
                String identifier = tokens[0];
                tokens = tokens[1].split("\"");
                if (tokens.length == 2) {
                    String expression = tokens[1];
                    //check each line to assign attributes of the evidence
                    if (identifier.equals(nameIdentifier)) { //check for name
                        name = expression;
                    }
                    else
                        if (identifier.equals(weightIdentifier)) {
                            weight = getDoubleFromString(expression, weight);
                        }
                    else
                            if (identifier.equals(biasIdentifier)) {
                                bias = getDoubleFromString(expression, bias);
                            }
                    else
                                if (identifier.equals(intervalsIdentifier)) {
                                    intervals = getIntervalsFromString(expression, intervals);
                                }
                    else
                                    if (identifier.equals(existsIdentifier)) {
                                        exists = getBooleanFromString(expression, exists);
                                    }
                    else
                                        if (identifier.equals(necessaryIdentifier)) {
                                            necessary = getBooleanFromString(expression, necessary);
                                        }
                    else
                                            if (identifier.equals(dependenciesIdentifier)) {
                                                dependencies = getDependenciesFromString(expression, dependencies);
                                            }
                }
            }

            line = br.readLine();
        }

        return name == null ? null : new EvidenceAndDependencies(
                new Evidence(name,
                        weight,
                        necessary,
                        new Evidence.IntervalTree(intervals != null ? intervals : new Evidence.Interval(Integer.MIN_VALUE, Integer.MAX_VALUE).toArrayList()),
                        bias,
                        exists),
                dependencies);
    }

    /**
     *
     * @param s the string containing the value
     * @param defaultValue returned if a {@link NumberFormatException} is thrown
     * @return int value of s or defaultValue
     */
    private static int getIntFromString(String s, int defaultValue) {
        try {
            int n = Integer.valueOf(s);
            return n;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     *
     * @param s the string containing the value
     * @param defaultValue returned if a {@link NumberFormatException} is thrown
     * @return double value of s or defaultValue
     */
    private static double getDoubleFromString(String s, double defaultValue) {
        try {
            double n = Double.valueOf(s);
            return n;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     *
     * @param s the string containing the value
     * @param defaultValue returned if a {@link NumberFormatException} is thrown
     * @return boolean value of s or defaultValue
     */
    private static boolean getBooleanFromString(String s, boolean defaultValue) {
        try {
            boolean n = Boolean.valueOf(s);
            return n;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     * Parses an expression containing {@link Evidence.Interval}(s) then returns the resulting intervals in a List
     * @param s the string containing the intervals
     * @param defaultValue returned if errors occurred while parsing s
     * @return List of Intervals value of s or default value
     */
    private static List<Evidence.Interval> getIntervalsFromString(String s, List<Evidence.Interval> defaultValue) {
        String intervals = s;
        ArrayList<Evidence.Interval> intervalList = new ArrayList<Evidence.Interval>();
        boolean moreIntervals = true;
        try {
            do {
                int start = intervals.indexOf("(");
                if (start > -1) {
                    int end = intervals.indexOf(")", start + 1);
                    if (end > -1) {
                        String interval = intervals.substring(start + 1, end);
                        int separator = interval.indexOf(",");
                        String stringStart = interval.substring(0, separator).toLowerCase();
                        int intStart = stringStart.equals("-inf") ? Integer.MIN_VALUE : Integer.valueOf(stringStart);
                        String stringEnd = interval.substring(separator + 1, interval.length()).toLowerCase();
                        int intEnd = stringEnd.equals("+inf") ? Integer.MAX_VALUE : Integer.valueOf(stringEnd);
                        Evidence.Interval realInterval = new Evidence.Interval(intStart, intEnd);
                        intervalList.add(realInterval);
                        intervals = intervals.substring(end+1);
                        if (intervals.length() > 0 && intervals.charAt(0) == ',') {
                            intervals = intervals.substring(1);
                        }
                        else
                            moreIntervals = false;
                    }
                    else
                        return defaultValue;
                }
                else
                    return defaultValue;
            } while(moreIntervals);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
        return intervalList;
    }

    /**
     * Parses an expression containing {@link DependencyAttributes}(s) then returns the resulting attributes in a List
     * @param s the string containing the dependencies and attributes
     * @param defaultValue returned if errors occurred while parsing s
     * @return List of DependencyAttributes value of s or default value
     */
    private static List<DependencyAttributes> getDependenciesFromString(String s, List<DependencyAttributes> defaultValue) {
        String dependencies = s;
        ArrayList<DependencyAttributes> dependencyList = new ArrayList<DependencyAttributes>();
        boolean moreDependencies = true;
        try {
            do {
                int start = dependencies.indexOf("(");
                if (start > -1) {
                    int end = dependencies.indexOf(")", start + 1);
                    if (end > -1) {
                        String dependency = dependencies.substring(start + 1, end);
                        int separator = dependency.indexOf(",");
                        String name = dependency.substring(0, separator);
                        int separator2 = dependency.indexOf(",", separator+1);
                        String type = dependency.substring(separator + 1, separator2);
                        boolean value = Boolean.valueOf(dependency.substring(separator2 + 1, dependency.length()));
                        DependencyAttributes attributes = new DependencyAttributes(name, type, value);
                        dependencyList.add(attributes);
                        dependencies = dependencies.substring(end+1);
                        if (dependencies.length() > 0 && dependencies.charAt(0) == ',') {
                            dependencies = dependencies.substring(1);
                        }
                        else
                            moreDependencies = false;
                    }
                    else
                        return defaultValue;
                }
                else
                    return defaultValue;
            } while(moreDependencies);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
        return dependencyList;
    }
}
