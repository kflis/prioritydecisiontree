/*
	Copyright Kevin Storchan-Flis
	11/12/2013
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.HashSet;

/**
 * Represents an entity that has its own list of current evidence and a list of goal evidence. The the current evidence
 * can be thought of what you know about but not necessarily have, and the goal evidence is what you want your current
 * evidence to match up to (in terms of existence), eventually.
 * <p>
 * Each agent also has subagents. These subagents represent the decisions that its parent can make. Subagents are generated
 * statically by calling {@link Agent#generateAgents(int)} or dynamically when calling
 * {@link Agent#getBestResultsDynamically(int, int, int)}}. The more agents you generate, the more accurate your results become.
 * Eventually, the subagents at the bottom of the tree (the children) are used to compare utility and overall usefulness
 * of results.
 * </p>
 */
public class Agent {
    private final List<Evidence> currentEvidence;
    private final List<Evidence> goalEvidence;
    private final int time;
    private ArrayList<Agent> subAgents = null;
    private static final long MAX_AGENTS = 10000000; //can change, but 10 million seems to be about 3-4gb of ram

    public Agent(List<Evidence> currentEvidence, List<Evidence> goalEvidence, int time) {
        this.currentEvidence = currentEvidence;
        this.goalEvidence = goalEvidence;
        this.time = time;
    }

    /**
     * @see Agent#generateAgents(int)
     * @return true if it is safe to generate more agents, false otherwise
     */
    public boolean isItSafeToGenerateMoreAgents(int groupSize) {
        long x = 0;
        if (subAgents != null) {
            for (Agent agent : subAgents) {
                x += agent.predictSubagents(groupSize);
                if (x >= MAX_AGENTS)
                    return false;
            }
            if (x + countAgents() >= MAX_AGENTS)
                return false;
        }
        else
            if (generateCombinations(groupSize).size() >= MAX_AGENTS)
                return false;
        return true;
    }

    private long predictSubagents(int groupSize) {
        long x = 0;
        if (subAgents == null) {
            return generateCombinations(groupSize).size();
        }
        for (Agent a : subAgents) {
            x += a.predictSubagents(groupSize);
        }
        return x;
    }

    /**
     * Generate new subAgents with all combinations of combinable evidences at this time based on the groupSize.
     * @param groupSize the combination size of evidences for the next batch of generated agents
     */
    public boolean generateAgents(int groupSize) {
        if (subAgents == null) {
            subAgents = new ArrayList<Agent>();
            HashSet<HashSet<Integer>> combos = generateCombinations(groupSize);
            if (combos.size() > 0) { //there exists combinations at this time
                ArrayList<Integer> necessaryEvidences = generateNecessaryEvidences();
                for (HashSet<Integer> combo : combos) {
                    boolean pruneFlag = false; //used for checking if this combination is valid at this time
                    ArrayList<Evidence> agentEvidence = new ArrayList<Evidence>(currentEvidence);
                    for (Integer i : combo)
                        agentEvidence.set(i, agentEvidence.get(i).setExistence(goalEvidence.get(i).doesExist()));
                    for (Integer i : necessaryEvidences) {
                        if (agentEvidence.get(i).doesExist() != goalEvidence.get(i).doesExist())
                            pruneFlag = true;
                    }
                    if (!pruneFlag) //only add subagents that are valid combinations with no deadline conflicts
                        subAgents.add(new Agent(agentEvidence, goalEvidence, time+1));
                }
            }
            else { //no decisions can be made at this time, but still try to prune decisions that are do not contain
                    //necessary evidences
                boolean pruneFlag = false;
                Agent agent = new Agent(currentEvidence, goalEvidence, time+1);
                ArrayList<Integer> necessaryEvidences = agent.generateNecessaryEvidences();
                for (Integer i : necessaryEvidences) {
                    if (agent.currentEvidence.get(i).doesExist() != agent.goalEvidence.get(i).doesExist())
                        pruneFlag = true;
                }
                if (!pruneFlag) //only add subagents that are valid combinations with no deadline conflicts
                    subAgents.add(agent);
            }
        }
        else {
            for (int i = subAgents.size()-1; i >= 0; i--) {
                Agent agent = subAgents.get(i);
                boolean remove = agent.generateAgents(groupSize);
                if (remove) //if all subagents of this subagent are pruned, prune this subagent
                    subAgents.remove(i);
            }

        }

        return subAgents.size() == 0;
    }

    private int countAgents() {
        return countAgents(this);
    }

    private int countAgents(Agent agent) {
        int total = 1;
        if (agent.subAgents != null)
            for (Agent a : agent.subAgents)
                total += countAgents(a);
        return total;

    }

    public int countCombinations() {
        return countCombinations(this);
    }

    private int countCombinations(Agent agent) {
        if (agent.subAgents == null)
            return 1;
        int total = 0;
        for (Agent a : agent.subAgents)
            total += countCombinations(a);
        return total;

    }

    /**
     * Generate the indexes of necessary {@link Agent#goalEvidence} that must be completed at the current time
     * @return the indexes of necessary evidences
     */
    private ArrayList<Integer> generateNecessaryEvidences() {
        ArrayList<Integer> necessaryEvidences = new ArrayList<Integer>();
        for (int i = 0; i < goalEvidence.size(); i++) {
            if (goalEvidence.get(i).isNecessaryAndHitDeadline(time))
                necessaryEvidences.add(i);

        }
        return necessaryEvidences;
    }

    /**
     * Generate all possible combinations of decisions to be made at this time. This takes into account dependencies
     * and intervals of {@link Evidence} in {@link Agent#currentEvidence} of evidences that do equal their respective
     * existence in {@link Agent#goalEvidence}
     * @param groupSize the combination size of evidences for the next batch of generated agents
     * @return a set of a set of all combinations that can be made as decisions at this time
     */
    private HashSet<HashSet<Integer>> generateCombinations(int groupSize) {
        ArrayList<Integer> combinableEvidences = new ArrayList<Integer>(); //the indexes of current evidences that are both modifiable and different than the goal evidences
        ArrayList<ArrayList<Integer>> combinationList = new ArrayList<ArrayList<Integer>>(); //the list of all valid combination sets of indexes
        //generate the list of combinableEvidences
        for (int i = 0; i < currentEvidence.size(); i++) {
            Evidence evidence = currentEvidence.get(i);
            Evidence goal = goalEvidence.get(i);
            if (evidence != null && evidence.isModifiable(time) && evidence.doesExist() != goal.doesExist())
                combinableEvidences.add(i);
        }

        //use the list of combinable evidences to generate all combinations for combinationList
        HashSet<HashSet<Integer>> combos = new HashSet<HashSet<Integer>>();
        HashSet<HashSet<Integer>> previousCombos = new HashSet<HashSet<Integer>>();
        HashSet<Integer> tempList = new HashSet<Integer>();
        if (combinableEvidences.size() <= groupSize) {
            if (combinableEvidences.size() == 0)
                return combos;
            for (Integer i : combinableEvidences)
                tempList.add(i);
            HashSet<Integer> toRemove = new HashSet<Integer>();
            for (int i : tempList) { //get the evidences in this combination
                Evidence evidence = currentEvidence.get(i);
                ArrayList<Evidence.Dependency> dependencies =  evidence.getDependencies();
                if (dependencies != null) {  //if dependencies exist for this evidence
                    for (Evidence.Dependency dependency : dependencies) { //check if all dependencies are satisfied
                        if (dependency.isThereAConflict(tempList, currentEvidence))
                            toRemove.add(i);
                    }
                }
            }
            for (int i : toRemove)
                tempList.remove(i);
            combos.add(tempList);
            return combos;
        }
        for (int i = 0; i < groupSize; i++) {
           tempList.add(combinableEvidences.get(i));
        }
        combos.add(tempList);
        previousCombos.add(tempList);

        //this generates all combinations of decisions to be made at this time, based on the groupSize
        //i.e. if there are 20 combinable evidences and a groupSize of 3, there will be 20 Choose 3 combinations before
        //removing invalid combinations
        for (int i = groupSize; i < combinableEvidences.size(); i++) {
            for (HashSet<Integer> combo : combos) {
                Integer [] comboArray = new Integer[combo.size()];
                int z = 0;
                for (Integer x : combo) //copy current combo to an array
                    comboArray[z++] = x;
                for (int x = 0; x < comboArray.length; x++) { //generate permutations for this combo
                    Integer [] tempCombo = new Integer[comboArray.length];
                    System.arraycopy(comboArray, 0, tempCombo, 0, comboArray.length);
                    tempCombo[x] = combinableEvidences.get(i);
                    HashSet<Integer> newCombo = new HashSet<Integer>();
                    for (Integer n : tempCombo)
                        newCombo.add(n);
                    previousCombos.add(newCombo); //add the permutation to a hash set, where it becomes a combination
                }
            }
            for (HashSet<Integer> combo : previousCombos)
                combos.add(combo);
            previousCombos.clear();
        }

        HashSet<HashSet<Integer>> combosToRemove = new HashSet<HashSet<Integer>>();

        //for every valid combination of evidences, check the dependencies of each combination and its evidences
        for (HashSet<Integer> combo : combos) {
            for (int i : combo) { //get the evidences in this combination
                Evidence evidence = currentEvidence.get(i);
                ArrayList<Evidence.Dependency> dependencies =  evidence.getDependencies();
                boolean remove = false;
                if (dependencies != null) {  //if dependencies exist for this evidence
                    for (Evidence.Dependency dependency : dependencies) { //check if all dependencies are satisfied
                        if (dependency.isThereAConflict(combo, currentEvidence))
                            combosToRemove.add(combo);
                    }
                }
            }
        }

        for (HashSet<Integer> combo : combosToRemove)
            combos.remove(combo);

        return combos;
    }

    /**
     * Returns the best results without permanently growing the tree
     * @see Agent#exploreForResults(int, int, int, java.util.Stack, java.util.ArrayList)
     */
    public  ArrayList<UtilityTuple> getBestResultsDynamically(int groupSize, int timeInFuture, int results) {
        ArrayList<UtilityTuple> tuples = new ArrayList<UtilityTuple>();
        Stack<Agent> history = new Stack<Agent>();
        Agent tempAgent = new Agent(currentEvidence, goalEvidence, time);
        history.add(tempAgent);
        exploreForResults(groupSize, timeInFuture, results, history, tuples);
        ArrayList<UtilityTuple> reversed = new ArrayList<UtilityTuple>();
        for (int i = tuples.size()-1; i >= 0; i--)
            reversed.add(tuples.get(i));
        return reversed;
    }

    /**
     * Combines functionality of {@link Agent#generateAgents(int)} and {@link Agent#getBestResults(int)} without
     * growing the tree.
     * @param groupSize the combination size of evidences for the next batch of generated agents
     * @param timeInFuture how far in the future the tree will look ahead
     * @param results max results returned
     * @param history contains previously visited agents
     * @param tuples our "best" results
     */
    public void exploreForResults(int groupSize, int timeInFuture, int results, Stack<Agent> history, ArrayList<UtilityTuple> tuples) {
        if (timeInFuture <= 0) { //stop exploring deeper in the tree
            Agent a = history.peek();
            double util = 0;
            for (int i = 0; i < a.goalEvidence.size() && i < a.currentEvidence.size(); i++) {
                Evidence evidence = a.currentEvidence.get(i);
                if (evidence.doesExist() == a.goalEvidence.get(i).doesExist())
                    util += evidence.getUtility();
            }
            UtilityTuple tuple = new UtilityTuple(a, util);
            ArrayList<Agent> historyArray = new ArrayList<Agent>(history);
            for (int i = historyArray.size()-2; i >= 0; i--)
                tuple.add(historyArray.get(i));

            if (tuples.size() < results) {
                tuples.add(tuple);
                Collections.sort(tuples);
            }
            else {
                if (tuple.getUtility() > tuples.get(0).getUtility()) {
                    tuples.set(0, tuple);
                    Collections.sort(tuples);
                }
            }
        }
        else { //explore deeper in the tree
            Agent a = history.peek();
            a.generateAgents(groupSize);
            for (int i = a.subAgents.size()-1; i >= 0; i--) {
                history.push(a.subAgents.remove(i));
                exploreForResults(groupSize, timeInFuture-1, results, history, tuples);
                history.pop();
            }
        }
    }

    /**
     * Return the best decisions to be made based on growing the tree using {@link Agent#generateAgents(int)}
     * @param number the max results returned
     * @return a list of containers holding their utility and respective decisions
     */
    public ArrayList<UtilityTuple> getBestResults(int number) {
        ArrayList<UtilityTuple> results = getResults(number);
        ArrayList<UtilityTuple> reversed = new ArrayList<UtilityTuple>();
        for (int i = results.size()-1; i >= 0; i--)
            reversed.add(results.get(i));
        return reversed;
    }

    /**
     * Helper method to recursively retrieve the best results in the tree
     */
    private ArrayList<UtilityTuple> getResults(int number) {
        ArrayList<UtilityTuple> tuples = new ArrayList<UtilityTuple>();
        if (subAgents == null) {
            double util = 0;
            for (int i = 0; i < goalEvidence.size() && i < currentEvidence.size(); i++) {
                Evidence evidence = currentEvidence.get(i);
                if (evidence.doesExist() == goalEvidence.get(i).doesExist())
                    util += evidence.getUtility();
            }
            UtilityTuple tuple = new UtilityTuple(this, util);
            tuples.add(tuple);
            return tuples;
        }
        else {
            UtilityTuple smallestTuple = null;
            for (Agent agent : subAgents) {
                ArrayList<UtilityTuple> subTuples = agent.getResults(number);
                for (UtilityTuple tuple : subTuples) {
                    if (tuples.size() < number) {
                        tuple.add(this);
                        tuples.add(tuple);
                        Collections.sort(tuples);
                        smallestTuple = tuples.get(0);

                    }
                    else
                    if (tuple.getUtility() > smallestTuple.getUtility()) {
                        tuple.add(this);
                        tuples.set(0, tuple);
                        Collections.sort(tuples);
                        smallestTuple = tuples.get(0);
                    }
                }
            }
            return tuples;
        }
    }

    /**
     * Handy container to store a list of a sets of decisions with the respective utility of this list
     */
    public static final class UtilityTuple implements Comparable<UtilityTuple> {
        private AgentNode first;
        private final double utility;

        public UtilityTuple(Agent agent, double utility) {
            this.first = new AgentNode(agent);
            this.utility = utility;
        }

        @Override
        public String toString() {
            String output = "Utility: " + getUtility() + "\n";
            String decisions = "";
            for (ArrayList<String> list : decisionList())
                decisions += list + " -> ";
            decisions = decisions.substring(0, decisions.length()-4);
            return output + decisions;
        }

        public void add(Agent agent) {
            AgentNode node = new AgentNode(agent, first);
            first = node;
        }

        /**
         * @return the utility of this list of decisions
         */
        public double getUtility() {
            return utility;
        }

        /**
         * @return the list of sets of decisions that are were created as time progressed
         */
        public ArrayList<ArrayList<String>> decisionList() {
            AgentNode node = first;
            ArrayList<ArrayList<String>> decisions = new ArrayList<ArrayList<String>>();
            while (node.next != null) {
                List<Evidence> currentEvidence = node.agent.currentEvidence;
                node = node.next;
                List<Evidence> nextEvidence = node.agent.currentEvidence;
                ArrayList<String> changes = new ArrayList<String>();
                int numOfChanges = 0;
                for (int i = 0; i < currentEvidence.size(); i++) {
                    Evidence evidence = currentEvidence.get(i);
                    Evidence next = nextEvidence.get(i);
                    if (evidence.doesExist() != next.doesExist()) {
                        numOfChanges++;
                        changes.add(next.toString());
                    }
                }
                decisions.add(changes);
            }
            return decisions;
        }

        @Override
        public int compareTo(UtilityTuple o) {
            if (utility < o.utility)
                return -1;
            if (utility > o.utility)
                return 1;
            return 0;
        }

        public static class AgentNode {
            private final Agent agent;
            private final AgentNode next;

            public AgentNode(Agent agent){
                this(agent, null);
            }

            public AgentNode(Agent agent, AgentNode next) {
                this.agent = agent;
                this.next = next;
            }

        }
    }
}
