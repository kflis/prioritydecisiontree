/*
	Copyright Kevin Storchan-Flis
	11/12/2013
 */
import java.util.ArrayList;
import java.util.List;

/**
 *  A special kind of decision tree that takes into account the following attributes for each decision: name, weight, bias,
 *  necessary, intervals, and dependencies. These attributes are stored in {@link Evidence}, which contains information
 *  to help make decisions and a discrete value denoting whether that evidence exists or not. We use  the existence of evidence
 *  to allow dependant decisions to be made.
 *  <p>
 *  We start out constructing the tree from a goal evidence list using that is read from a text file
 *  using {@link PriorityDecisionTree#createFromFile(String,String)}(and a starting evidence list if specified).
 *  The goal evidence list contains the states of the evidence we want. The starting evidence contains the states
 *  of the evidence we start out with. There are two ways to retrieve results:
 *  {@link PriorityDecisionTree#grow(int)} then {@link PriorityDecisionTree#generateBestDecisions(int)} or
 *  {@link PriorityDecisionTree#generateBestDecisionsDynamically(int, int, int)} </p>
 *
 */
public class PriorityDecisionTree {
    private Agent rootAgent;

    /**
     * Constructs a decision tree without starting evidence manually set
     * @see PriorityDecisionTree#PriorityDecisionTree(java.util.List, java.util.List, int)
     */
    public PriorityDecisionTree(List<Evidence> goalEvidence, int currentTime) {
        this(null, goalEvidence, currentTime);
    }

    /**
     * Constructs a decision tree with starting evidence (if set) and goal evidence.
     *
     * @param startingEvidence the evidence states we start out with
     * @param goalEvidence the evidence states that we want
     * @param currentTime the time we start out with
     */
    public PriorityDecisionTree(List<Evidence> startingEvidence, List<Evidence> goalEvidence, int currentTime) {
        List<Evidence> currentEvidence = startingEvidence;
        if (currentEvidence == null) {
            currentEvidence = new ArrayList<Evidence>();
            for (Evidence evidence : goalEvidence)
                currentEvidence.add(evidence.setExistence(false));
        }
        else {
            currentEvidence = new ArrayList<Evidence>(startingEvidence);
        }
        this.rootAgent = new Agent(currentEvidence, goalEvidence, currentTime);
    }

    /**
     * Creates a PriorityDecisionTree from reading a goal evidence text file only
     * @see PriorityDecisionTree#createFromFile(String, String)
     */
    public static PriorityDecisionTree createFromFile(String goalEvidencePath) {
        return createFromFile(null, goalEvidencePath);
    }

    /**
     * Creates a PriorityDecisionTree from reading a starting evidence text file (if specified) and a goal
     * evidence text file.
     * <br></br>
     * <p>For the goal evidence, we use the following format: <br></br>
     * - use #EVIDENCE# to create a new evidence<br></br>
     * - use #END# to denote the end of an evidence's attributes<br></br>
     * - required attributes:<br></br>
     *      name="some unique name"<br></br>
     * - default attributes:<br></br>
     *      weight="1.0"<br></br>
     *      bias="1.0"<br></br>
     *      intervals="(-inf,+inf)"<br></br>
     *      exists="true"<br></br>
     *      necessary="false"<br></br>
     *      dependencies="" </p>
     * <br></br>
     * <p>For the existence starting evidence, we use the following format for each line: <br></br>
     * some name=boolvalue<br></br>
     * Do not use quotes, extra spaces, or anything else in the document. Evidence that starts out as false
     * does not need to be specified. By default, the starting evidence will contain everything in the goal
     * evidence set to false
     * </p>
     * @param startingEvidencePath the location of the starting evidence
     * @param goalEvidencePath the location of the goal evidence
     * @return a newly constructed PriorityDecisionTree based on the evidence read from the text file(s)
     */
    public static PriorityDecisionTree createFromFile(String startingEvidencePath, String goalEvidencePath) {
        TextToEvidence.ResultsContainer evidenceResults = TextToEvidence.getEvidenceFromFile(startingEvidencePath, goalEvidencePath);
        return new PriorityDecisionTree(evidenceResults.startingEvidence, evidenceResults.goalEvidence, 0);
    }

    /**
     * Grow the decision tree with the specified size of combinations per decision
     * @param combinations the size of combinations per decision
     */
    public void grow(int combinations) {
        rootAgent.generateAgents(combinations);
    }

    /**
     *
     * @return the total number of combinations of decisions that can be made at the current level of the tree
     */
    public int decisionCombinationCount() {
        return rootAgent.countCombinations();
    }

    /**
     * Check if the tree will not grow with unacceptable memory consumption then grow it if it is safe
     * @see PriorityDecisionTree#grow(int)
     * @return whether or not the tree was successfully grown
     */
    public boolean safelyGrow(int combinations){
        if (rootAgent.isItSafeToGenerateMoreAgents(combinations)) {
            rootAgent.generateAgents(combinations);
            return true;
        }
        return false;
    }

    /**
     * Return the best decisions to be made based on growing the tree using {@link PriorityDecisionTree#grow(int)}
     * @param numOfDecisions the max results returned
     * @return a list of containers holding their utility and respective decisions
     */
    public List<Agent.UtilityTuple> generateBestDecisions(int numOfDecisions) {
        return rootAgent.getBestResults(numOfDecisions);
    }

    /**
     * Dynamically generate the best decisions without growing the tree using the provided starting and goal
     * evidence lists. Combines functionality of {@link PriorityDecisionTree#grow(int)} and
     * {@link PriorityDecisionTree#generateBestDecisions(int)}
     * @param combinations the size of combinations per decision
     * @param timeInFuture how far in the future the tree will look ahead
     * @param numOfDecisions the max results returned
     * @return a list of containers holding their utility and respective decisions
     */
    public List<Agent.UtilityTuple> generateBestDecisionsDynamically(int combinations, int timeInFuture, int numOfDecisions) {
        return rootAgent.getBestResultsDynamically(combinations, timeInFuture, numOfDecisions);
    }



}
