/*
	Copyright Kevin Storchan-Flis
	11/12/2013
 */
import java.util.*;

/**
 * Evidence represents the existence of something you know about the world. It has attributes
 * that can allow or disallow the existence to be modified. When it exists, it can provide
 * utility that can be used to help choose the best decisions.
 */
public class Evidence {
    private final String name;
    private final double weight;
    private final double bias;
    private final IntervalTree intervalTree;
    private final int deadline;
    private final boolean necessary;
    private final boolean exists;
    private ArrayList<Dependency> dependencies;
    private Evidence reversedEvidence = null; //stores a copy of itself with its state reversed to save memory when
                                              //calling setExistence() to avoid many deep copies

    //used for instantiating the reversed existence state of an evidence
    private Evidence(Evidence e) {
        this.name = e.name;
        this.weight = e.weight;
        this.bias = e.bias;
        this.intervalTree = e.intervalTree;
        this.deadline = e.deadline;
        this.necessary = e.necessary;
        this.exists = !e.exists;
        this.dependencies = e.dependencies;
        this.reversedEvidence = e;
    }

    /**
     * Creates an evidence.
     * @param name what this evidence represents
     * @param weight the significance of this evidence
     * @param necessary if this evidence must be completed within its time intervals
     * @param intervalTree when the evidence can be modified
     * @param bias the bias towards this evidence existing
     * @param exists whether or not this evidence exists
     */
    public Evidence(String name, double weight, boolean necessary, IntervalTree intervalTree, double bias, boolean exists) {
        if (weight == 0 || bias == 0)
            throw new IllegalArgumentException("@Evidence::Evidence(String name, double weight, boolean necessary, IntervalTree intervalTree, double bias, boolean exists)" +
                    " Cannot assign a bias or weight to 0");
        this.name = name;
        this.weight = weight;
        this.bias = bias;
        this.necessary = necessary;
        this.exists = exists;
        this.intervalTree = intervalTree;
        deadline = intervalTree.getDeadline();
    }

    /**
     * Only call this when setting up the initial lists of evidences!
     * @param dependencies what this evidence is dependant on
     */
    public void setDependencies(ArrayList<Dependency> dependencies) {
        this.dependencies = dependencies;
    }

    /**
     *
     * @return what this evidence is dependant on
     */
    public ArrayList<Dependency> getDependencies() {
        return dependencies;
    }

    public String getName() { return name; }

    @Override
    public String toString() {
        return name + "=" + exists;
    }

    /**
     * @param value the existence you want to change this evidence to
     * @return a reference of this evidence with the specified existence
     */
    public Evidence setExistence(boolean value) {
        if (reversedEvidence == null)
            reversedEvidence = new Evidence(this);
        return exists == value ? this : reversedEvidence;
    }

    /**
     *
     * @param time the current time
     * @return true if you can modify the existence of this evidence, false otherwise
     */
    public boolean isModifiable(int time) {
        return intervalTree.isInInterval(time);
    }

    /**
     * Used for pruning sub optimal decisions and making necessary decisions in time
     * @param time the current time
     * @return true if this cannot be changed after the current time and is necessary
     */
    public boolean isNecessaryAndHitDeadline(int time) {
        return time == deadline && necessary;
    }

    public boolean doesExist() {
        return exists;
    }

    public double getUtility() {
        return exists ? (weight * bias) : 0;
    }

    /**
     * Stores the intervals for when an evidence can be modified
     */
    public static class IntervalTree {
        private IntervalNode root;

        public IntervalTree(List<Interval> intervalList) {
            List<Interval> intervals = intervalList;
            Collections.sort(intervals);
            root = generateTree(intervals, 0, intervals.size() - 1);
        }

        private IntervalNode generateTree(List<Interval> intervals, int start, int end) {
            if (start > end || intervals == null)
                return null;
            int pos = ((end - start) / 2) + start;
            IntervalNode node = new IntervalNode(intervals.get(pos),
                                                 generateTree(intervals, start, pos-1),
                                                 generateTree(intervals, pos+1, end));
            return node;
        }

        public int getDeadline() {
            if (root == null)
                return Integer.MAX_VALUE;
            IntervalNode node = root;
            while (node.getRight() != null)
                node = node.getRight();
            return node.getEnd();
        }

        public boolean isInInterval(int n) {
            return isInInterval(root, n);
        }

        private boolean isInInterval(IntervalNode node, int n) {
            if (node == null)
                return false;
            int i = node.inBounds(n);
            if (i == 0)
                return true;
            if (i < 0)
                return isInInterval(node.getLeft(), n);
            return isInInterval(node.getRight(), n);
        }

        private static class IntervalNode {
            private Interval interval;
            private IntervalNode left;
            private IntervalNode right;

            public IntervalNode(Interval interval, IntervalNode left, IntervalNode right) {
                this.interval = interval;
                this.left = left;
                this.right = right;
            }

            public IntervalNode getLeft() {
                return left;
            }

            public IntervalNode getRight() {
                return right;
            }

            public int inBounds(int n) {
                return interval.inBounds(n);
            }

            public int getStart() {
                return interval.getStart();
            }

            public int getEnd() {
                return interval.getEnd();
            }
        }
    }

    /**
     * Stores an interval
     */
    public static class Interval implements  Comparable<Interval> {
        private final int start;
        private final int end;

        public Interval(int start) {
            this(start, Integer.MAX_VALUE);
        }

        public Interval(int start, int end) {
            if (start > end)
                throw new IllegalArgumentException("@Interval::Interval(int start, int end)" +
                            " the start of an interval must be <= to the end");
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return start;
        }

        public int getEnd() {
            return end;
        }

        public int inBounds(int n) {
            if (n >= start && n <= end)
                return 0;
            if (n < start)
                return -1;
            return 1;
        }

        @Override
        public int compareTo(Interval o) {
            if (start >= o.start)
                if (end <= o.end) {
                    if (o.start == start && o.end == end)
                        return 0;
                    if (start > o.start)
                        return 1;
                    return -1;
                }
                else
                    return 1;
            else
                return -1;
        }

        @Override
        public String toString(){
            return "[" + start + ", " + end + "]";
        }

        public ArrayList<Interval> toArrayList () {
            ArrayList<Interval> list = new ArrayList<Interval>();
            list.add(this);
            return list;
        }
    }

    /**
     * Stores the dependencies of each evidence
     */
    public static class Dependency {
        public final int index;
        public final boolean value;
        public final boolean requirement;
        public final boolean corequirement;

        public Dependency(int index, boolean value, boolean requirement, boolean corequirement) {
            this.index = index;
            this.value = value;
            this.requirement = requirement;
            this.corequirement = corequirement;
        }

        /**
         *	Check if this dependency for the evidence that its associated with does not have any
         * conflicts with past evidences and the evidence in the current combo
         * @param currentCombo indices of evidence where existence changes are about to be made
         * @param pastEvidences the state of evidences in the past
         * @return
         */
        public boolean isThereAConflict(HashSet<Integer> currentCombo, List<Evidence> pastEvidences) {
            if (requirement) {  //if the current evidence has a requirement
                if (pastEvidences.get(index).doesExist() != value) //make sure the current evidence satisfies the requirement
                    return !(corequirement && currentCombo.contains(index));
            }
            else
                if (corequirement) {        // if the current evidence has a corequirement
                    if (currentCombo.contains(index)) {   //and the combination contains the corequirement
                        if (pastEvidences.get(index).doesExist() == value)    //make sure the changed value equals the dependency's value
                            return true;
                    }
                    else
                        return true;

                }

            return false;
        }
    }
}
