/*
	Copyright Kevin Storchan-Flis
	11/12/2013
 */
import java.util.*;

public class TestClass {

    public static void main(String [] args) {
    	//these tests use 4 combinations per decision
        PriorityDecisionTree tree = PriorityDecisionTree.createFromFile(
                "CoursesGoalEvidence.txt");
        int j = 1;
        //test dynamic generation over 6 intervals
        for (int z = 1; z < 7; z++) {
        	long time = System.currentTimeMillis();
	        for (Agent.UtilityTuple tuple : tree.generateBestDecisionsDynamically(4, z, 5)) {
	            //System.out.println("Result: " + j++ + " Utility: " + tuple.getUtility());
	            int x = 1;
//	            for (ArrayList<String> list : tuple.decisionList())
//	                System.out.println("\tTime " + x++ + ": " + list);
	        }
	        System.out.println("\nElapsed time when dynamically generating 4 combinations each interval for " + z + " interval(s)"
	        		+ " and for the 5 best decisions is " + elapsed(time) /(double) 1000 + "s\n");
        }

        //test growing and retrieving decisions over 6 intervals
        for (int z = 1; z < 7; z++) {
            long time = System.currentTimeMillis();
            tree.grow(4);
            double growTime = elapsed(time) /(double) 1000 ;
            System.out.println("\nElapsed time when permanently generating 4 combinations for interval " + z + " is "
	        		+ growTime + "s\n");
            time = System.currentTimeMillis();
            tree.generateBestDecisions(5);
            double decisionTime = elapsed(time) /(double) 1000 ;
            System.out.println("\nElapsed time when finding the best 5 decisions for interval " + z + " is "
	        		+ decisionTime /(double) 1000 + "s\n");
            System.out.println("\nTotal time = " + (growTime + decisionTime));
            System.out.println("Total combinations = " + tree.decisionCombinationCount());
            
        }
        int i = 1;
        //can use tree.generateBestDecisionsDynamically(4, 6, 5) instead to test that
        for (Agent.UtilityTuple tuple : tree.generateBestDecisions(7)) {
            System.out.println("Result: " + i++ + " Utility: " + tuple.getUtility());
            int x = 1;
            for (ArrayList<String> list : tuple.decisionList())
                System.out.println("\tTime " + x++ + ": " + list);
        }
        System.out.println(tree.decisionCombinationCount());


    }
    
    public static long elapsed(long start) {
    	return System.currentTimeMillis() - start;
    }
}
